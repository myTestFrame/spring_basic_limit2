package com.xd.dao;

import com.xd.model.UserInfo;

import java.util.List;

public interface UserMapper {
    List<UserInfo> selectAll();
    int getCount();
    int selectByPassword(UserInfo userInfo);
    UserInfo selectByLoginId(String loginId);
    void insert(UserInfo userInfo);

}
